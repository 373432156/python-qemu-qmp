Error classes
=============

.. automodule:: qemu.qmp.error
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource
