.. qemu documentation master file, created by
   sphinx-quickstart on Mon Dec 13 16:50:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :caption: Home
   :maxdepth: 3
   :hidden:

   main

.. include:: ../README.rst

.. toctree::
   :maxdepth: 4
   :caption: Documentation

   overview

.. toctree::
   :maxdepth: 4
   :caption: Module Reference

   qemu.qmp.error
   qemu.qmp.events
   qemu.qmp.legacy
   qemu.qmp.message
   qemu.qmp.protocol
   qemu.qmp.qmp_client
   qemu.qmp.util

.. toctree::
   :maxdepth: 2
   :caption: Tool Reference

   qmp_shell
   qmp_shell_wrap


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
